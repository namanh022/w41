import React from "react";

const Interval = ({ time, setTime }) => {
  const addTime = () => {
    let min = new Date().getMinutes();
    let hour = new Date().getHours();
    let second = new Date().getSeconds();
    let milisecond = new Date().getMilliseconds();
    if(min < 10){
        min = `0${min}`
    }
    if(hour < 10){
        hour = `0${hour}`
    }
    if(second < 10){
        second = `0${second}`
    }
    if(milisecond < 10){
        milisecond = `00${milisecond}`
    } else if(milisecond < 100){
        milisecond = `0${milisecond}`
    }
    const dateNow = `${hour}:${min}:${second}-${milisecond}`
    setTime(time.concat(dateNow))
  };

  return <button onClick={addTime} style={{fontSize: '36px'}}>Interval time</button>;
};

export default Interval;
